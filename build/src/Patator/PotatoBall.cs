﻿namespace MyMod.Weapon
{
    using DuckGame;
    using System;
    using System.Collections.Generic;

    public class PotatoBall : PhysicsObject
    {
        private int reboundCount;
        private Duck _owner;

        public PotatoBall(float xpos, float ypos, Duck owner)
          : base(xpos, ypos)
        {
            this.center = new Vec2(4f, 4f);
            this.collisionOffset = new Vec2(-4f, -4f);
            this.collisionSize = new Vec2(8f, 8f);
            this.depth = (Depth)(-0.5f);
            this.thickness = 2f;
            this.weight = 0.5f;
            this.bouncy = 0.5f;
            this._owner = owner;
            this._impactThreshold = 0.01f;
            this.reboundCount = 3;
            this.graphic = new Sprite(GetPath("potato"));
        }

        public override void Update()
        {
            if ((double)Math.Abs(this.hSpeed) + (double)Math.Abs(this.vSpeed) > 0.10000000149011612)
                this.angleDegrees = -Maths.PointDirection(Vec2.Zero, new Vec2(this.hSpeed, this.vSpeed));
            if (this.isServerForObject)
            {
                if (this.grounded && (double)Math.Abs(this.vSpeed) + (double)Math.Abs(this.hSpeed) <= 0.20000000298023224)
                    this.alpha -= 0.2f;
                if ((double)this.alpha <= 0.0)
                    Level.Remove((Thing)this);
            }
            base.Update();
        }

        public override void OnSoftImpact(MaterialThing with, ImpactedFrom from)
        {
            if (Network.isActive && this.connection != DuckNetwork.localConnection || this.removeFromLevel)
                return;

            bool rebound = false;
            Duck duck = with as Duck;
            if (duck != null && duck.ragdoll == null && duck._trapped == null)
            {
                duck.hSpeed = this.hSpeed * 1.5f;
                duck.vSpeed = this.vSpeed * 1.5f + 0.5f;
                if (duck.holdObject != null)
                {
                    Thing holdObject = (Thing)duck.holdObject;
                    duck.ThrowItem();
                    this.Fondle(holdObject);
                }
                this.Fondle((Thing)duck);
                duck.GoRagdoll();

                rebound = true;
            }

            RagdollPart ragdollPart = with as RagdollPart;
            if (ragdollPart != null && ragdollPart.doll != null)
            {
                this.Fondle((Thing)ragdollPart.doll);
                if (ragdollPart.doll.part2 != null && ragdollPart.doll.part1 != null)
                {
                    ragdollPart.doll.part2.hSpeed = this.hSpeed * 1.5f;
                    ragdollPart.doll.part2.vSpeed = this.vSpeed * 1.5f + 0.5f;
                    ragdollPart.doll.part1.hSpeed = this.hSpeed * 1.5f;
                    ragdollPart.doll.part1.vSpeed = this.vSpeed * 1.5f + 0.5f;
                }

                rebound = true;
            }

            if (rebound)
            {
                if (this.reboundCount <= 0)
                    Level.Remove((Thing)this);

                this.hSpeed *= -0.8f;
                this.vSpeed *= -0.8f;
                this.reboundCount--;
            }
        }
    }
}
