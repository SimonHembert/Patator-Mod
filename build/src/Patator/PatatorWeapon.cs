﻿namespace MyMod.Weapon
{
    using DuckGame;

    [EditorGroup("Patator")]
    public class PatatorWeapon : Gun
    {
        public bool readyToFire;
        private SpriteMap potatorMap;

        public PatatorWeapon(float xval, float yval) : base(xval, yval)
        {
            this.ammo = 4;
            this._ammoType = (AmmoType)new ATPotatoBall();
            this.potatorMap = new SpriteMap(GetPath("patator_sheet"), 46, 7);
            this.potatorMap.speed = 0.0f;
            this.graphic = (Sprite)this.potatorMap;
            this.center = new Vec2(15f, 2f);
            this.collisionOffset = new Vec2(-4f, -2f);
            this.collisionSize = new Vec2(30f, 5f);
            this._barrelOffsetTL = new Vec2(50f, -5f);

            this.weight = 1.5f;
            this._type = "gun";
            this._fireSound = "shotgunFire2";
            this._kickForce = 6f;
            this._fireRumble = RumbleIntensity.Light;
            this._numBulletsPerFire = 6;
            this._manualLoad = true;
            this.flammable = 1f;
            this._holdOffset = new Vec2(0.0f, -2f);
            this._editorName = "Potator";
            this.editorTooltip = "My life is potato. Take a potato and leave the show!";
            this.loaded = false;
            this.isFatal = false;
            this._clickSound = "campingEmpty";
            this.physicsMaterial = PhysicsMaterial.Plastic;
        }

        public override void OnPressAction()
        {
            if (this.ammo <= 0)
            {
                this.DoAmmoClick();
                return;
            }

            if (this.readyToFire)
            {
                this.ammo--;
                if (this.duck != null)
                    RumbleManager.AddRumbleEvent(this.duck.profile, new RumbleEvent(this._fireRumble, RumbleDuration.Pulse, RumbleFalloff.None));
                SFX.Play("campingThwoom");
                this.ApplyKick();
                Vec2 vec2 = this.Offset(this.barrelOffset);
                for (int index = 0; index < 6; ++index)
                {
                    CampingSmoke campingSmoke = new CampingSmoke((float)((double)this.barrelPosition.x - 8.0 + (double)Rando.Float(8f) + (double)this.offDir * 8.0), this.barrelPosition.y - 8f + Rando.Float(8f));
                    campingSmoke.depth = (Depth)(float)(0.89999997615814209 + (double)index * (1.0 / 1000.0));
                    if (index < 3)
                        campingSmoke.move -= this.barrelVector * Rando.Float(0.05f);
                    else
                        campingSmoke.fly += this.barrelVector * (1f + Rando.Float(2.8f));
                    Level.Add((Thing)campingSmoke);
                }
                if (!this.receivingPress)
                {
                    PotatoBall potato = new PotatoBall(vec2.x, vec2.y - 2f, this.duck);
                    Level.Add((Thing)potato);
                    this.Fondle((Thing)potato);
                    //if (this.onFire)
                    //    potato.LightOnFire();
                    if (this.owner != null)
                        potato.responsibleProfile = this.owner.responsibleProfile;
                    potato.clip.Add(this.owner as MaterialThing);
                    potato.hSpeed = this.barrelVector.x * 10f;
                    potato.vSpeed = (float)((double)this.barrelVector.y * 7.0 - 0.75);
                }

                this.readyToFire = false;
            }
            else
            {
                this.potatorMap.frame = 5 - this.ammo;
                SFX.Play("click");
                this.readyToFire = true;
            }
        }
    }
}
