﻿namespace MyMod.Weapon
{
    using DuckGame;
    using System;
    using System.Collections.Generic;

    public class ATPotatoBall : AmmoType
    {
        public ATPotatoBall()
        {
            this.accuracy = 0.6f;
            this.range = 115f;
            this.penetration = 1f;
            this.rangeVariation = 10f;
            this.combustable = false;
        }

        public override void PopShell(float x, float y, int dir)
        {
            ShotgunShell shotgunShell = new ShotgunShell(x, y);
            shotgunShell.hSpeed = (float)dir * (1.5f + Rando.Float(1f));
            Level.Add((Thing)shotgunShell);
        }
    }
}
