﻿namespace MyMod.Weapon
{
    using DuckGame;
    using System.Collections.Generic;

    [EditorGroup("Patator")]
    public class ExplosiveBananaCluster : ExplosiveBanana
    {
        private SpriteMap _sprite;
        int _sparkAmount = 5;
        bool _isLit;
        private Sound _burnSound;
        private ActionTimer _litTimer;
        private float _radius = 40f;

        public ExplosiveBananaCluster(float xval, float yval, bool explodeOnImpact = false)
          : base(xval, yval, explodeOnImpact)
        {
            this._ammoType = (AmmoType)new ATShrapnel();
            this._type = "gun";
            this._sprite = new SpriteMap("banana", 16, 16);
            this._sprite.frame = 4;
            this.graphic = (Sprite)this._sprite;
            this.center = new Vec2(8f, 8f);
            this.collisionOffset = new Vec2(-6f, -4f);
            this.collisionSize = new Vec2(12f, 11f);
            this._holdOffset = new Vec2(0.0f, 2f);
            this.friction = 0.05f;
            this.editorTooltip = "This is a new type of body-positive organic banana, but also explosive.";
        }

        protected override void BananaExplode()
        {
            int bananaSpawnCount = 5;
            for (int bananaIndex = 0; bananaIndex < bananaSpawnCount; bananaIndex++)
            {
                Vec2 bananaOffset = new Vec2(Rando.Float(-2f, 2f), Rando.Float(-1f, -6f));
                ExplosiveBanana banana = new ExplosiveBanana(this.position.x + bananaOffset.x, this.position.y + bananaOffset.y, true);
                banana.hSpeed = bananaOffset.x * 1.2f;
                banana.vSpeed = bananaOffset.y * 1.2f;
                banana.Lit();
                Level.Add(banana);
            }

            base.BananaExplode();
        }
    }
}
