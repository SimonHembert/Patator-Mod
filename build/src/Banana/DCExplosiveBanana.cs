﻿namespace MyMod.Weapon
{
    using DuckGame;
    using System;
    using System.Collections.Generic;

    public class DCExplosiveBanana : DeathCrateSetting
    {
        public override void Activate(DeathCrate c, bool server = true)
        {
            float x = c.x;
            float ypos = c.y - 2f;
            Level.Add((Thing)new ExplosionPart(x, ypos));
            int num1 = 6;
            if (Graphics.effectsLevel < 2)
                num1 = 3;
            for (int index = 0; index < num1; ++index)
            {
                float deg = (float)index * 60f + Rando.Float(-10f, 10f);
                float num2 = Rando.Float(12f, 20f);
                Level.Add((Thing)new ExplosionPart(x + (float)Math.Cos((double)Maths.DegToRad(deg)) * num2, ypos - (float)Math.Sin((double)Maths.DegToRad(deg)) * num2));
            }
            if (server)
            {
                for (int index = 0; index < 10; ++index)
                {
                    ExplosiveBananaCluster banana = new ExplosiveBananaCluster(c.x, c.y);
                    banana.hSpeed = (float)((double)((float)index / 9f) * 40.0 - 20.0) * Rando.Float(0.5f, 1f);
                    banana.vSpeed = Rando.Float(-3f, -11f);
                    banana.Lit();
                    Level.Add((Thing)banana);
                }
                Level.Remove((Thing)c);
            }
            Graphics.FlashScreen();
            SFX.Play("explode");
            RumbleManager.AddRumbleEvent(c.position, new RumbleEvent(RumbleIntensity.Heavy, RumbleDuration.Short, RumbleFalloff.Medium));
        }
    }
}
