﻿namespace MyMod.Weapon
{
    using DuckGame;
    using System.Collections.Generic;

    [EditorGroup("Patator")]
    public class ExplosiveBanana : Gun
    {
        private SpriteMap _sprite;
        int _sparkAmount = 5;
        bool _isLit;
        private Sound _burnSound;
        private ActionTimer _litTimer;
        private ActionTimer _canExplodeTimer;
        private float _radius = 40f;
        private bool _explodeOnImpact = false;

        public ExplosiveBanana(float xval, float yval, bool explodeOnImpact = false)
          : base(xval, yval)
        {
            this.ammo = 1;
            this._ammoType = (AmmoType)new ATShrapnel();
            this._type = "gun";
            this._sprite = new SpriteMap("banana", 16, 16);
            this.graphic = (Sprite)this._sprite;
            this.center = new Vec2(8f, 13f);
            this.collisionOffset = new Vec2(-6f, -3f);
            this.collisionSize = new Vec2(12f, 5f);
            this._fireRumble = RumbleIntensity.Kick;
            this._holdOffset = new Vec2(-1f, 2f);
            this.bouncy = 0.4f;
            this.friction = 0.05f;
            this.physicsMaterial = PhysicsMaterial.Rubber;
            this.editorTooltip = "One banana can cause a lot of trouble.";
            this.isFatal = false;
            this._explodeOnImpact = explodeOnImpact;
            this._isLit = false;
        }

        public override void Update()
        {
            base.Update();

            if (!this._isLit && this.duck != null)
            {
                this.Lit();
            }

            if (this._isLit)
            {
                Vec2 vec2_1 = this.Offset(new Vec2(-6f, -4f));
                for (int sparkIndex = 0; sparkIndex < this._sparkAmount; sparkIndex++)
                {
                    Level.Add((Thing)Spark.New(vec2_1.x, vec2_1.y, new Vec2(Rando.Float(-1f, 1f), -0.5f), 0.1f));
                }

                Level.Add((Thing)SmallSmoke.New(vec2_1.x, vec2_1.y - 6f));
                if ((bool)this._litTimer || this.grounded && this._explodeOnImpact && this._canExplodeTimer != null && (bool)this._canExplodeTimer)
                    this.BananaExplode();
            }
        }

        protected virtual void BananaExplode()
        {
            if (this._burnSound != null)
            {
                this._burnSound.Stop();
                this._burnSound = (Sound)null;
            }

            bool pExplode = true;
            int num = 0;
            foreach (Window window in Level.CheckCircleAll<Window>(this.position, this._radius - 20f))
            {
                Thing.Fondle((Thing)window, DuckNetwork.localConnection);
                if (Level.CheckLine<Block>(this.position, window.position, (Thing)window) == null)
                    window.Destroy((DestroyType)new DTImpact(this));
            }
            foreach (PhysicsObject t in Level.CheckCircleAll<PhysicsObject>(this.position, this._radius + 30f))
            {
                if (this.isLocal && this.owner == null)
                    Thing.Fondle((Thing)t, DuckNetwork.localConnection);
                if ((double)(t.position - this.position).length < 30.0)
                    t.Destroy((DestroyType)new DTImpact(this));

                ExplosiveBanana explosiveBanana = t as ExplosiveBanana;
                if (explosiveBanana == null)
                {
                    t.sleeping = false;
                    t.vSpeed = -2f;
                }
            }

            HashSet<ushort> varBlocks = new HashSet<ushort>();
            foreach (BlockGroup blockGroup1 in Level.CheckCircleAll<BlockGroup>(this.position, this._radius))
            {
                if (blockGroup1 != null)
                {
                    BlockGroup blockGroup2 = blockGroup1;
                    List<Block> blockList = new List<Block>();
                    foreach (Block block in blockGroup2.blocks)
                    {
                        if (Collision.Circle(this.position, this._radius - 22f, block.rectangle))
                        {
                            block.shouldWreck = true;
                            AutoBlock autoBlock = block as AutoBlock;
                            if (autoBlock != null && !autoBlock.indestructable)
                            {
                                varBlocks.Add(autoBlock.blockIndex);
                                if (pExplode && num % 10 == 0)
                                {
                                    Level.Add((Thing)new ExplosionPart(block.x, block.y));
                                    //Level.Add((Thing)SmallFire.New(block.x, block.y, Rando.Float(-2f, 2f), Rando.Float(-2f, 2f)));
                                }
                                ++num;
                            }
                        }
                    }
                    blockGroup2.Wreck();
                }
            }
            foreach (Block block in Level.CheckCircleAll<Block>(this.position, this._radius - 22f))
            {
                AutoBlock autoBlock = block as AutoBlock;
                VerticalDoor verticalDoor = block as VerticalDoor;

                if (autoBlock != null && !autoBlock.indestructable)
                {
                    block.skipWreck = true;
                    block.shouldWreck = true;
                    varBlocks.Add((block as AutoBlock).blockIndex);
                    if (pExplode)
                    {
                        if (num % 10 == 0)
                        {
                            Level.Add((Thing)new ExplosionPart(block.x, block.y));
                            //Level.Add((Thing)SmallFire.New(block.x, block.y, Rando.Float(-2f, 2f), Rando.Float(-2f, 2f)));
                        }
                        ++num;
                        continue;
                    }
                }
                if (verticalDoor != null)
                {
                    Level.Remove((Thing)block);
                    block.Destroy((DestroyType)new DTRocketExplosion((Thing)null));
                }
            }
            if (Network.isActive && (this.isLocal || this.isServerForObject) && varBlocks.Count > 0)
                Send.Message((NetMessage)new NMDestroyBlocks(varBlocks));
            foreach (ILight light in Level.current.things[typeof(ILight)])
                light.Refresh();

            SFX.Play("explode");
            Level.Remove((Thing)this);
        }

        public override void Fire()
        {
        }

        public override void Terminate()
        {
            if (this._burnSound != null)
                this._burnSound.Kill();
            base.Terminate();
        }

        public virtual void Lit()
        {
            this._isLit = true;
            this._burnSound = SFX.Play("fuseBurn", 0.5f, looped: true);
            this._litTimer = new ActionTimer(0.1f, 12f);
            if (this._explodeOnImpact)
                this._canExplodeTimer = new ActionTimer(0.5f, 2f);
        }

        public override void OnSoftImpact(MaterialThing with, ImpactedFrom from)
        {
            base.OnSoftImpact(with, from);
            if (this._isLit && this._explodeOnImpact && (bool)this._canExplodeTimer)
            {
                ExplosiveBanana banana = with as ExplosiveBanana;
                //Block block = with as Block;
                //BlockGroup blockGroup = with as BlockGroup;
                if (banana == null)
                    this.BananaExplode();
            }
        }
    }
}
