﻿namespace MyMod.Weapon
{
    using DuckGame;
    using System.Collections.Generic;

    [EditorGroup("Patator")]
    public class FireHolster : Holster
    {
        public bool _on;

        public FireHolster(float xpos, float ypos) : base(xpos, ypos)
        {
            this._overPart = new SpriteMap(GetPath("fireHolster_over"), 8, 10);
            this._overPart.center = new Vec2(4f, 4f);
            this._underPart = new SpriteMap(GetPath("fireHolster_under"), 9, 22);
            this._underPart.center = new Vec2(13f, 17f);
            this._sprite = new SpriteMap(GetPath("fireHolster"), 17, 23);
            this.graphic = (Sprite)this._sprite;
            this.collisionOffset = new Vec2(-2f, -2f);
            this.collisionSize = new Vec2(10f, 20f);
            this.center = new Vec2(5f, 5f);
            this._jumpMod = true;
            this.backOffset = -11f;
            this.editorTooltip = "A holster that can be used as a jetpack.";
        }

        public override void Update()
        {
            base.Update();

            if (this.equippedDuck != null)
            {
                if (this.equippedDuck.inputProfile.Pressed("JUMP") && !this.equippedDuck.grounded)
                    this.PressAction();

                if (this.equippedDuck.inputProfile.Released("JUMP"))
                    this.ReleaseAction();
            }

            if (this._on && !this.held)
                this.HoldAction();
        }

        public override void OnHoldAction()
        {
            base.OnHoldAction();
            if (this.containedObject != null)
                this.containedObject.HoldAction();
        }

        public override void OnPressAction()
        {
            base.OnPressAction();
            if (!this._on)
            {
                this._on = true;
                if (this.containedObject != null)
                    this.containedObject.PressAction();
            }
        }

        public override void OnReleaseAction()
        {
            base.OnReleaseAction();
            if (this._on)
            {
                this._on = false;
                if (this.containedObject != null)
                    this.containedObject.ReleaseAction();
            }
        }

        //protected override void DrawParts()
        //{
        //    base.DrawParts();
        //}
    }
}
