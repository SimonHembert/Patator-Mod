﻿namespace MyMod.Weapon
{
    using DuckGame;
    using System;
    using System.Collections.Generic;

    public class PotatoBullet : Bullet
    {
        public PotatoBullet(
          float xval,
          float yval,
          AmmoType type,
          float ang = -1f,
          Thing owner = null,
          bool rbound = false,
          float distance = -1f,
          bool tracer = false,
          bool network = true)
          : base(xval, yval, type, ang, owner, rbound, distance, tracer, network)
        {

        }

        protected override void OnHit(bool destroyed)
        {
            if (!destroyed || !this.isLocal)
                return;

            IEnumerable<Duck> ducks = Level.CheckCircleAll<Duck>(this.position, 20f);
            foreach (Duck duck in ducks)
            {
                duck.hSpeed = this.hSpeed * 0.5f;
                duck.vSpeed = this.vSpeed * 0.5f;
                duck.GoRagdoll();
                this.ReverseTravel();
            }
        }
    }
}
