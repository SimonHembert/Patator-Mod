﻿namespace MyMod.Weapon
{
    using DuckGame;
    using System;
    using System.Collections.Generic;

    public class ATPotato : AmmoType
    {
        public ATPotato(string spritePath)
        {
            this.accuracy = 1f;
            this.penetration = 0.35f;
            this.bulletSpeed = 9f;
            this.rangeVariation = 0.0f;
            this.speedVariation = 0.0f;
            this.range = 2000f;
            this.rebound = true;
            this.affectedByGravity = true;
            this.deadly = false;
            this.weight = 5f;
            this.ownerSafety = 4;
            this.bulletThickness = 2f;
            this.bulletColor = Color.White;
            this.bulletType = typeof(PotatoBullet);
            this.immediatelyDeadly = true;
            this.sprite = new Sprite(spritePath);
            this.sprite.CenterOrigin();
            this.flawlessPipeTravel = true;
        }
    }
}
